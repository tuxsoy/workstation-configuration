# Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Setup Workstation Configuration
#
# Usage: sh setup.sh

USER=tuxsoy

# Install Required Packages
sudo -u root apk add \
    `# GUI` \
    eudev \
    sway sway-doc \
    xorg-server-xwayland \
    swaylock swaylock-doc \
    swayidle swayidle-doc \
    rofi rofi-doc \
    waybar waybar-doc \
    pango \
    \
    `# Command Line Utilities` \
    exa exa-doc \
    less less-doc \
    git git-doc \
    curl curl-doc \
    man \
    \
    `# Applications` \
    alacritty alacritty-doc \
    firefox-esr \
    flatpak \
    keepassxc keepassxc-doc \
    gotop \
    chromium \
    mpv mpv-doc \
    \
    `# Text Editor` \
    nodejs npm xsel \
    neovim neovim-doc \
    \
    `# System Services` \
    openssh openssh-doc \
    udisks2 udisks2-doc \
    sudo sudo-doc \
    dbus dbus-doc \
    wireless-tools wireless-tools-doc \
    iwd iwd-doc \
    dhcpcd dhcpcd-doc \
    bluez bluez-doc \
    brightnessctl brightnessctl-doc \
    \
    `# Sound` \
    alsa-utils alsa-utils-doc alsa-lib alsaconf \
    pulseaudio pulseaudio-alsa alsa-plugins-pulse \
    pulseaudio-utils pavucontrol pulseaudio-bluez \
    \
    `# Fonts` \
    font-noto

# Setup Services
sudo -u root rc-update add iwd
sudo -u root rc-update add dhcpcd

# Setup Profile
sudo -u ${USER} echo "export ENV=${HOME}/.config/env" > ${HOME}/.profile

# Symlink Configuration Files
sudo -u ${USER} ln -sf `pwd`/config/alacritty ${HOME}/.config
sudo -u ${USER} ln -sf `pwd`/config/env ${HOME}/.config/env
sudo -u ${USER} ln -sf `pwd`/config/fontconfig ${HOME}/.config
sudo -u ${USER} ln -sf `pwd`/config/git ${HOME}/.config
sudo -u ${USER} ln -sf `pwd`/config/nvim ${HOME}/.config
sudo -u ${USER} ln -sf `pwd`/config/sway ${HOME}/.config
sudo -u ${USER} ln -sf `pwd`/config/waybar ${HOME}/.config
