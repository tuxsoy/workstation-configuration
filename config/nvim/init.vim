" Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
"
" This program is free software: you can redistribute it and/or modify
" it under the terms of the GNU Affero General Public License as
" published by the Free Software Foundation, either version 3 of the
" License, or (at your option) any later version.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU Affero General Public License for more details.
"
" You should have received a copy of the GNU Affero General Public License
" along with this program.  If not, see <https://www.gnu.org/licenses/>.

" Neovim Configuration

" ------------------------------------------------------------------------------
"  Plugins

call plug#begin(stdpath('data') . '/plugged')

Plug 'Yggdroot/indentLine'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'easymotion/vim-easymotion'
Plug 'liuchengxu/vim-which-key'
Plug 'liuchengxu/vista.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ntpeters/vim-better-whitespace'
Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'

call plug#end()

" ------------------------------------------------------------------------------
"  Behavior

set mouse=n
set nobackup
set noshowmode
set nowritebackup
set number
set smartcase
set textwidth=80
set timeoutlen=0
set updatetime=0

let g:mapleader = "\<Space>"

" ------------------------------------------------------------------------------
"  Appearance

set cmdheight=2
set hidden
set list
set listchars=tab:>>
set signcolumn=yes
set t_Co=256
set termguicolors

color dracula

" ------------------------------------------------------------------------------
" EasyMotion

let g:EasyMotion_smartcase = 1

" ------------------------------------------------------------------------------
" Vista

let g:vista_default_executive = 'coc'
let g:vista_echo_cursor_strategy = 'floating_win'

" ------------------------------------------------------------------------------
" COC

autocmd CursorHold * silent call CocActionAsync('highlight')

" ------------------------------------------------------------------------------
" Better Whitespace

let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

" ------------------------------------------------------------------------------
" NERDCommenter

let g:NERDSpaceDelims = 1
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDDefaultAlign = 'left'

" ------------------------------------------------------------------------------
" Airline

let g:airline#extensions#tabline#enabled = 1

" ------------------------------------------------------------------------------
" Which Key

call which_key#register('<Space>', "g:which_key_map")

nnoremap <silent> <leader> :<c-u>WhichKey '<Space>'<CR>
vnoremap <silent> <leader> :<c-u>WhichKeyVisual '<Space>'<CR>

let g:which_key_map =  {}

let g:which_key_map.f = {
  \ 'name' : '+file',
  \ 'o' : ['CtrlP', 'Open'],
  \ 's' : ['update', 'Save'],
  \ 't' : ['NERDTreeToggle', 'Tree'],
  \ }

let g:which_key_map.b = {
  \ 'name' : '+buffer',
  \ 'd' : ['bd', 'Delete'],
  \ 'n' : ['bnext', 'Next'],
  \ 'p' : ['bprevious', 'Previous'],
  \ 'l' : ['CtrlPBuffer', 'List'],
  \ 'c' : ['<Plug>NERDCommenterToggle', 'Comment'],
  \ 's' : {
    \ 'name' : '+search',
    \ 'c' : ["<Plug>(easymotion-s)", '1 Character'],
    \ 't' : ["<Plug>(easymotion-s2)", '2 Characters'],
    \ 'n' : ["<Plug>(easymotion-sn)", 'N Characters'],
    \ 'f' : ["<Plug>(easymotion-sl)", 'Line'],
    \ 'l' : ["<Plug>(easymotion-j)", 'Jump Down'],
    \ 'k' : ["<Plug>(easymotion-k)", 'Jump Up'],
    \ },
  \ }

let g:which_key_map.l = {
  \ 'name' : '+lsp',
  \ 'g' : {
    \ 'name' : '+goto',
    \ 'd' : ["<Plug>(coc-definition)", 'Definition'],
    \ 'y' : ["<Plug>(coc-type-definition)", 'Type Definition'],
    \ 'i' : ["<Plug>(coc-implementation)", 'Implementation'],
    \ 'r' : ["<Plug>(coc-references)", 'References'],
    \ },
  \ 'r' : ["<Plug>(coc-rename)", 'Rename'],
  \ 'f' : ["<Plug>(coc-format)", 'Format'],
  \ 't' : [":Vista!!", 'Symbol Tree'],
  \ 'd' : {
    \ 'name' : '+diagnostics',
    \ 'i' : ["<Plug>(coc-diagnostic-info)", 'Info'],
    \ 'n' : ["<Plug>(coc-diagnostic-next)" , 'Next'],
    \ 'p' : ["<Plug>(coc-diagnostic-prev)", 'Previous'],
    \ },
  \ }

let g:which_key_map.v = {
  \ 'name' : '+vcs',
  \ 's' : ['Gstatus', 'Status'],
  \ 'd' : ['Gdiffsplit', 'Diff'],
  \ 'b' : ['Gblame', 'Blame'],
  \ 'c' : ['Gcommit', 'Commit'],
  \ 'o' : ['Gread', 'Read'],
  \ 'w' : ['Gwrite', 'Write'],
  \ }

" ------------------------------------------------------------------------------
